import React from 'react';
import Link from 'next/link';
import DataCards from '../src/Mocks/DataCards';
import Card from '../src/components/Card';
import UserPainelLayout from '../src/components/UserPainelLayout';
import styles from '../styles/Home.module.scss';

export default function Home() {
  return (
    <UserPainelLayout>
      <div className={styles.cardsWrapper}>
        <div className={styles.cardContainer}>
          {DataCards.map((item) => {
            return (
              <Link href={item.linkTo}>
                <a>
                  <Card item={item} />
                </a>
              </Link>
            );
          })}
        </div>
      </div>
    </UserPainelLayout>
  );
}
