import React from 'react';
import UserPainelLayout from '../../src/components/UserPainelLayout';
import patientCares from '../../src/Mocks/patientsCares';
import styles from '../../styles/PatientCares.module.scss';

const patientCaresOrientation = () => {
  return (
    <UserPainelLayout>
      <div className={styles.textContainer}>
        <h1>Avaliação do estado geral do idoso</h1>
        <div>
          {patientCares.map((care) => {
            const careName = care.getCareName();
            return (
              <div key={careName}>
                <h2>{careName}</h2>
                {care.getFormattedText()}
              </div>
            );
          })}
        </div>
      </div>
    </UserPainelLayout>
  );
};

export default patientCaresOrientation;
