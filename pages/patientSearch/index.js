import React, { useState } from 'react';
import UserPainelLayout from '../../src/components/UserPainelLayout';
import SearchBar from '../../src/components/SearchBar';
import styles from '../../styles/PatientsSearch.module.scss';

const patientSearch = () => {
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = (results) => {
    // TODO: Conectar com API para executar a busca de pacientes e redirecionamento
    setSearchResults(results);
  };

  return (
    <UserPainelLayout>
      <div className={styles.screenContainer}>
        <div className={styles.searchContainer}>
          <SearchBar
            searchResults={searchResults}
            handleSearch={handleSearch}
            placeholder={'Nome/CPF do paciente'}
          ></SearchBar>
          {searchResults.length !== 0 && (
            <div className={styles.searchResults}>
              <ul>
                {searchResults.map((result) => (
                  <li
                    key={result.cpf}
                    onClick={() => this.handleItemClicked(place)}
                  >
                    <p className={styles.searchResult__patientName}>
                      {result.patientName}
                    </p>
                    <p className={styles.searchResult__patientCPF}>
                      CPF: {result.cpf}
                    </p>
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>
      </div>
    </UserPainelLayout>
  );
};

export default patientSearch;
