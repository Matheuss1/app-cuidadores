import React, { useState } from "react";
import LoginBox from "../../src/components/LoginBox";
import InputField from "../../src/components/InputField";
import Button from "../../src/components/Button";
import styles from "../../styles/Login.module.scss";

const Sign_in = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const handleUsernameChange = (event) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <main className={`${styles["sign-in-page"]}`}>
      <div className={`${styles["login-form"]} ${styles["login-form-sm"]}`}>
        <LoginBox onSubmit={handleSubmit}>
          <InputField
            label="Usuário"
            value={username}
            onChange={handleUsernameChange}
          />
          <InputField
            type="password"
            label="Senha"
            value={password}
            onChange={handlePasswordChange}
          />
          <Button type="submit">Entrar</Button>
        </LoginBox>
      </div>
    </main>
  );
};

export default Sign_in;
