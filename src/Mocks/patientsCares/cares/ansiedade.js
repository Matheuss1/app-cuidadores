import Care from './Care';

export default new Care(
  'Humor/Ansiedade',
  (
    <>
      <p>
        Transtornos mentais são diversas doenças e condições que afetam o
        comportamento, o pensamento e o humor. Não obstante, a apresentação de
        transtornos mentais nos idosos é, na grande maioria das vezes, de
        maneira atípica, ou seja, diferente do adulto jovem, não demonstrando os
        mesmos sinais e sintomas que outras faixas etárias. A função do médico
        nos casos de transtornos de humor/afetividade é excluir outras causas e
        orientar quanto ao tratamento mais adequado, evitando medicações
        incorretas para o idoso.
      </p>
      <h3>Ansiedade</h3>
      <p>
        Os transtornos ansiosos, conhecidos popularmente como ‘ansiedade’, se
        caracterizam por medo/preocupação excessiva podendo ser crônico ou
        pontual, gerando dificuldades na rotina diária e stress. Pode-se
        apresentar também com fadiga (cansaço), tensão muscular, alteração de
        sono e inquietação. Recomenda-se procurar o geriatra ou psiquiatra (se
        não disponível, o médico da unidade de saúde) se estiver se sentindo
        ansioso ou bastante ansioso nos últimos 15 dias. Em pacientes não idoso
        o tempo para diagnóstico de ansiedade é, geralmente, de 6 meses, mas
        como o idoso pode apresentar quadros diferentes, pode ser indicado a
        consulta em menor tempo.
      </p>
      <h3>Desânimo/Depressão</h3>
      <p>
        A depressão, que muitas vezes é associada ao desânimo, é também
        caracterizada pela fadiga (diminuição da energia), raciocínio lento,
        sentimentos negativos, distúrbios do sono (dorme muito ou pouco) e perda
        de peso ou ganho de peso. Muitas vezes pode se apresentar no idoso com a
        perda de vontade de fazer tarefas que antes geravam prazer ou que era
        hábito do idoso fazer. Assim como na ansiedade, deve-se procurar um
        médico psiquiatra ou geriatra (se não disponível, o médico da unidade de
        saúde) após 15 dias da apresentação dos sintomas.
      </p>
      <h3>Transtorno bipolar</h3>
      <p>
        No transtorno bipolar, a pessoa alterna entre episódios depressivos e
        episódios de mania ou hipomania (felicidade exagerada e euforia).
        Frequentemente, o diagnóstico desse transtorno é realizado ao final da
        adolescência, mas há evidências que os sinais e sintomas possam aparecer
        em idosos com mais de 65 anos (8%). Os cuidadores geralmente percebem
        essa alternância de humor durante o episódio depressivo. Ao denotar o
        episódio depressivo, deve-se considerar levar o paciente ao geriatra ou
        psiquiatra (se não disponível, o médico da unidade de saúde) para
        avaliação.
      </p>
    </>
  )
);
