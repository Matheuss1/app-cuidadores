import Care from './Care';

export default new Care(
  'Orientação',
  (
    <>
      <ul>
        <li>
          sabe a data (dia do mês, mês, ano, dia da semana, datas especiais)
        </li>
        <li>reconhece o ambiente onde se encontra</li>
        <li>sabe quem é, com quem está falando, relações de parentesco</li>
        <li>
          desorientado no espaço onde está, não reconhece as pessoas - Procurar
          geriatra ou psiquiatra.
        </li>
      </ul>

      <p>
        Existem dois tipos de orientação: a orientação temporal e a orientação
        espacial. A orientação temporal diz respeito à capacidade do indivíduo
        de reconhecer qual é os dias da semana, a data, o mês e o ano corrente,
        qual a data comemorativa mais próxima, a duração de intervalos (noção de
        tempo curto ou longo). Já a orientação espacial refere-se à capacidade
        do idoso de determinar em que ambiente ou local ele está.
      </p>
      <p>
        Com o envelhecimento, essas capacidades podem diminuir, resultando em
        uma desorientação, confusão, sentimento de estar perdido, e tudo isso
        irá repercutir na autonomia, na qualidade de vida e na saúde do idoso.
        Aqui vão alguns exercícios que podem ser realizados com o idoso para
        estimular a sua orientação temporoespacial:
      </p>
      <p>
        Para a orientação temporal, o cuidador pode fazer uso do calendário. É
        ideal que ele tenha letras e números grandes e esteja fixado em um lugar
        ao alcance do idoso, onde ele consiga ver com facilidade, como na
        geladeira, no espelho do banheiro ou na parede do quarto. Colocando
        dentro do guarda-roupa, por exemplo, fará com que o idoso não o veja com
        facilidade. O cuidador deverá perguntar ao idoso “Hoje é dia _ do mês de
        _ do ano de _. O dia da semana é _”, e fazer um X no dia correspondente.
        Esse treinamento deve acontecer todos os dias, pelo menos três vezes ao
        dia.
      </p>
      <p>
        Por fim, para estimular a orientação especial o cuidador poderá realizar
        algumas perguntas, como “Onde nós estamos?” e em seguida oferecer
        algumas dicas verbais (“começa com…”), visuais (mostrar uma imagem da
        casa, da rua ou da cidade) ou do ambiente (indicar um ponto de
        referência). Incentive o idoso a dizer desde o país, a região, o estado,
        a cidade, o bairro, a rua, o espaço até o cômodo que ele estiver
        ocupando.
      </p>
      <p>
        Em caso de desorientação aguda, isto é, repentina e abrupta, busque
        IMEDIATAMENTE ajuda médica.
      </p>
    </>
  )
);
