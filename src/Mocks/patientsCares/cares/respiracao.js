import Care from './Care';

export default new Care(
  'Respiração',
  (
    <ul>
      <li>
        Respiração lenta -{'>'} avaliar se tomou as medicações corretamente e
        sono do idoso
      </li>
      <li>
        Respiração curta -{'>'} avaliar se sente dor, está com febre, sinal de
        infecção e procurar assistência médica
      </li>
      <li>Respiração acelerada -{'>'} procurar emergência</li>
      <li>
        Sensação de desconforto/dor para respirar -{'>'} procurar emergência{' '}
      </li>
      <li>
        Respirando sem dificuldade Necessidade de oxigênio -{'>'}
        procurar emergência, número do SAMU
      </li>
    </ul>
  )
);
