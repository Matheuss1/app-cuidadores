import Care from './Care';

export default new Care(
  'Estado geral',
  (
    <ul>
      <li>
        aparenta estar saudável -{'>'} manter cuidados habituais + mensagem de
        incentivo
      </li>
      <li>
        parece doente -{'>'} avaliar outros parâmetros e decidir procurar o
        posto de saúde mais próximo ou o geriatra
      </li>
      <li>parece muito doente -{'>'} procurar emergência, número do SAMU</li>
    </ul>
  )
);
