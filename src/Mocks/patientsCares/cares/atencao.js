import Care from './Care';

export default new Care(
  'Atenção',
  (
    <ul>
      <li>Dificuldade de concentração.</li>
      <li>Concentração ou capacidade de manter este foco.</li>
      <li>
        Focaliza o interesse incidentalmente de acordo com as demandas do
        ambiente.
      </li>
    </ul>
  )
);
