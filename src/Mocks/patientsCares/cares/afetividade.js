import Care from './Care';

export default new Care(
  'Humor/Afetividade',
  (
    <ul>
      <li>
        ansioso -{'>'} procurar geriatra ou psiquiatra se estiver se sentindo
        ansioso há 15 dias
      </li>
      <li>feliz</li>
      <li>desanimado -{'>'} procurar geriatra ou psiquiatra</li>
      <li>expressando dois sentimentos ao mesmo tempo</li>
      <li>mudança de humor fácil</li>
      <li>deprimido -{'>'} procurar geriatra ou psiquiatra</li>
    </ul>
  )
);
