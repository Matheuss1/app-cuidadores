import Care from './Care';

export default new Care(
  'Consciência = Estado de alerta, capacidade de responder a estímulos e reconhecimento de si e da realidade',
  (
    <>
      <ul>
        <li>Alerta</li>
        <li>
          Sonolência
          <p>
            Avaliar uso das medicações; procurar geriatra para substituir
            medicamentos se estiver usando benzodiazepínicos (como clonazepam,
            rivotril, alprazolam, etc), pois essas medicações podem causar
            quedas e riscos para o idoso.
          </p>
        </li>
        <li>
          Confuso (delirium)
          <p>Procurar imediatamente a emergência.</p>
        </li>
      </ul>
      <p>
        A consciência é o estado de lucidez em que o idoso se encontra. Inclui o
        reconhecimento da realidade externa ou de si mesmo em determinado
        momento e a capacidade de responder a estímulos. Podemos classificar
        níveis ou estados de consciência, e essa classificação é importante para
        agirmos frente a alterações que ameacem a vida do idoso, aumentando as
        chances de um tratamento adequado para o caso.
      </p>
      <p>
        Ressaltamos os seguintes:
        <ul>
          <li> Vigil: É o estado normal da consciência.</li>
          <li>
            Sonolência: há o rebaixamento dos níveis de consciência do paciente,
            entretanto, ele pode ser despertado com estímulos brandos. O idoso
            terá um pensamento, fala e movimentos mais lentos.
          </li>
          <li>
            Estupor: É um nível pouco mais profundo de sonolência no qual o
            idoso necessita de estímulos mais repetidos e fortes para retomar a
            consciência.
          </li>
          <li>Coma: o idoso não pode ser acordado.</li>
        </ul>
      </p>
      <p>
        Há também um rebaixamento leve a moderado do nível de consciência que
        pode ser acompanhado por desorientação no tempo e no espaço, dificuldade
        de concentração, agitação ou lentificação psicomotora, discurso ilógico,
        alucinações. A esse conjunto de sinais atribuímos o nome de delirium,
        considerado uma urgência médica.Essas alterações na maioria das vezes
        estão associadas a doenças sérias, e, portanto, uma vez identificadas
        devem imediatamente ser avaliadas por um profissional de saúde.
      </p>
    </>
  )
);
