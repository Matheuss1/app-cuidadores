import Care from './Care';

export default new Care(
  'Hábito intestinal',
  (
    <>
      <ul>
        <li> Diarreia -{'>'} procurar posto de saúde ou médico geriatra</li>
        <li>
          Prisão de ventre -{'>'} procurar posto de saúde ou médico geriatra
        </li>
        <li>Sangue/pus/muco nas fezes -{'>'} procurar emergência</li>
        <li>Evacuou normalmente</li>
        <li>
          Evacuou com esforço -{'>'} reavaliar alimentação e se o idoso está
          bebendo água suficiente
        </li>
        <li>
          Evacuou um menor número de vezes -{'>'} reavaliar alimentação e se o
          idoso está bebendo água suficiente
        </li>
      </ul>
      <p>
        O sistema gastrointestinal do idoso, assim como outros sistemas desse
        tipo de paciente, passa por mudanças esperadas em função do seu
        envelhecimento. Com o passar dos anos, há alteração do gosto dos
        alimentos, disfunção no esvaziamento do estômago e diminuição da camada
        de proteção do intestino. Assim, é importante reconhecer que o idoso é
        mais vulnerável a doenças gastrointestinais (esôfago, estômago,
        intestinos, reto e ânus) e possuir uma atenção maior acerca desse trato.
        Abaixo, foram listados os dois principais sinais e sintomas
        gastrointestinais e o que fazer na observação desses e como preveni-los.
      </p>

      <h3>Prevenção</h3>
      <p>
        Medidas genéricas são importantes para o combate de diversas doenças
        gastrointestinais. Sendo assim, melhorar os hábitos alimentares comendo
        mais frutas e verduras, manter a vacinação atualizada, cuidado no
        consumo de água (preferir água mineral ou água fervida/filtrada),
        aumentar ingesta hídrica (pelo menos 2 litros por dia), manter hábitos
        higiênicos e praticar exercícios físicos acompanhado de profissional de
        educação física são medidas excelentes para evitar o adoecimento do
        idoso.
      </p>
      <h3>Prisão de ventre</h3>
      <p>
        Antes de tudo, é importante lembrar que a prisão de ventre isoladamente
        não é um sinal clássico para neoplasias (câncer/CA), devendo ser
        observado outros sinais e sintomas para a suspeita desse grupo de
        doenças. No mais, a prisão de ventre (menos de 3 evacuações por semana
        com fezes ‘duras’) pode estar relacionado à alimentação, mas é
        importante haver avaliação de um profissional da saúde em uma unidade de
        saúde ou com médico geriatra.
      </p>
    </>
  )
);
