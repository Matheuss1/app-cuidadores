import Care from './Care';

export default new Care(
  'Senso-percepção',
  (
    <ul>
      <li>
        diz que está vendo algo que não condiz com a realidade -{'>'} PROCURAR
        GERIATRA, PSIQUIATRA OU EMERGÊNCIA
      </li>
      <li>
        vê ou ouve algo de uma forma distorcida a partir de algo que já existe
        na realidade -{'>'} PROCURAR GERIATRA, PSIQUIATRA OU EMERGÊNCIA
      </li>
    </ul>
  )
);
