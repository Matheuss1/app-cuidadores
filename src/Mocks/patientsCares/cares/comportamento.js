import Care from './Care';

export default new Care(
  'Comportamento/Atitude',
  (
    <ul>
      <li>agressivo</li>
      <li>reservado</li>
      <li>extrovertido</li>
      <li>desinibido</li>
      <li>preocupado</li>
      <li>comportamentos bizarros</li>
      <li>cooperativo</li>
      <li>isolado</li>
    </ul>
  )
);
