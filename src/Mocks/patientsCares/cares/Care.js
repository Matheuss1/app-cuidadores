export default class Care {
  constructor(careName, formattedText) {
    this.careName = careName;
    this.formattedText = formattedText;
  }

  getCareName() {
    return this.careName;
  }

  getFormattedText() {
    return <> {this.formattedText} </>;
  }
}
