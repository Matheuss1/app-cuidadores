import estadoGeral from './cares/estadoGeral';
import comportamento from './cares/comportamento';
import respiracao from './cares/respiracao';
import habitoIntestinal from './cares/habitoIntestinal';
import afetividade from './cares/afetividade';
import ansiedade from './cares/ansiedade';
import percepcao from './cares/percepcao';
import orientacao from './cares/orientacao';
import consciencia from './cares/consciencia';
import atencao from './cares/atencao';

const eldersCares = [
  estadoGeral,
  comportamento,
  respiracao,
  habitoIntestinal,
  afetividade,
  ansiedade,
  percepcao,
  orientacao,
  consciencia,
  atencao,
];

export default eldersCares;
