const DataCards = [
  {
    title: 'Pacientes',
    linkTo: 'patientSearch',
  },
  {
    title: 'Orientações: estado geral do idoso',
    linkTo: 'elders-general-conditions-assessment',
  },
  {
    title: 'Orientações: manejo do idoso',
    linkTo: 'patientSearch',
  },
];

export default DataCards;
