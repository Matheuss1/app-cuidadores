import React from "react";
import styles from "./styles.module.scss";

const Card = ({ item }) => {
  return (
    <button type="button" className={styles.buttonHome}>
      {item.title}
    </button>
  );
};

export default Card;
