import React from 'react';
import styles from './styles.module.scss';

const UserPainelLayout = ({ children }) => {
  return (
    <div>
      <div className={styles.lateralContainer}>
        <div className={styles.lateralContainer__Profile}>
          <img src="/assets/icons/doctor.svg" alt="doctor"></img>
          <p>Nome do cuidador</p>
        </div>
      </div>
      <div className={styles.lateralContainerButtons}>
        <button className={styles.exitButton}>Sair</button>
      </div>
      <div className={styles.screenContent}>{children}</div>
    </div>
  );
};

export default UserPainelLayout;
