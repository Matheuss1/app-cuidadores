import React from "react";

const Input = ({ value, type, className, onChange }) => {
  return (
    <input
      type={type}
      className={className}
      value={value}
      onChange={onChange}
    ></input>
  );
};

export default Input;
