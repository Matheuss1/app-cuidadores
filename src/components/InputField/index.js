import React from "react";
import Input from "../Input";
import styles from "./styles.module.scss";

const InputField = ({ label, value, type, onChange }) => {
  return (
    <div className={`${styles["text-field"]}`}>
      <label
        className={`${styles["text-field__label"]} ${styles["text-field__label-sm"]}`}
      >
        {label}
      </label>
      <Input
        className={`${styles["input"]}`}
        value={value}
        type={type}
        onChange={onChange}
      />
    </div>
  );
};

export default InputField;
