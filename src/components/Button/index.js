import React from "react";
import styles from "./styles.module.scss"

const Button = ({ children, type = "button", handleClick }) => {
  return (
    <button type={type} onClick={handleClick} className={`${styles.button}`}>
      {children}
    </button>
  );
};

export default Button;
