import React from "react";
import styles from "./styles.module.scss";

const LoginBox = ({ children, onSubmit }) => {
  return (
    <form
      onSubmit={onSubmit}
      className={`${styles["login-box"]} ${styles["login-box-sm"]}`}
    >
      <div className={`${styles["logo-area"]}`}></div>
      <p className={`${styles["login-form-top-text"]}`}>Login</p>
      <div className={`${styles["login-form-content"]}`}>{children}</div>
    </form>
  );
};

export default LoginBox;
