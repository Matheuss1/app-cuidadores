import React, { useState } from 'react';
import { IoMdSearch } from 'react-icons/io';
import styles from './styles.module.scss';

const SearchBar = ({ placeholder }) => {
  const [searchValue, setSearchValue] = useState('');

  return (
    <nav className={styles.searchbar}>
      {searchValue === '' && (
        <>
          <IoMdSearch className={styles.SearchIcon} />
        </>
      )}
      <input
        className={styles['AutocompletePlace-input']}
        type="text"
        value={searchValue}
        onChange={(e) => {
          setSearchValue(e.target.value);
        }}
        onFocus={(e) => (e.target.placeholder = '')}
        onBlur={(e) => (e.target.placeholder = placeholder)}
        placeholder={placeholder}
      ></input>
    </nav>
  );
};

export default SearchBar;
